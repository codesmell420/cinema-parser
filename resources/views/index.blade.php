@extends('layouts.base')
@section('content')

<div class="outer">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1> Афиша Кинотеатра Салават</h1>
            </div>
            <div class="col">
                <form class="form-inline my-2 my-lg-0 justify-content-end" method="GET" action="{{ route('getData')}}">
                    <input class="form-control mr-sm-2" type="search" aria-label="Search" name="search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
                </form>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Название</th>
                    <th scope="col">Зал</th>
                    <th scope="col">Цена</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($filmScreenings as $filmScreening)
                    <tr>
                        <th scope="row">{{ $filmScreening->id }}</th>
                        <td> {{ $filmScreening->date }} </td>
                        <td> {{ $filmScreening->film_name }} </td>
                        <td> {{ $filmScreening->cinema_hall }}</td>
                        <td> {{ $filmScreening->price }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection