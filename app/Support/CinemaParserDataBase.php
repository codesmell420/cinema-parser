<?php

namespace App\Support;

use App\FilmScreening;
use Carbon\Carbon;
use App\Support\CinemaParser;

class CinemaParserDataBase
{
    /**
     * @param string $dateDB
     * @param string $dataParsed
     * 
     * Проверка актуальности переданной даты
     * $date, $dataParsed - строка даты
     * Возвращает true, если данные акктуальны, и false в противном случае
     * Если есть данные в бд, то проверяем их акктуальность  (дата(любая) != спрашенной дате(любой))
     * 
     * @return boolean
     */
    protected static function validateDate($dateDB, $dataParsed) {
        $dateDBCarbon = Carbon::parse($dateDB);
        $dataParsedCarbon = Carbon::createFromFormat("j F Y H:i", $dataParsed);
        if($dateDBCarbon->lt($dataParsedCarbon)){
            return true;
        }
        return false;
    }

    /**
     * @param array $filmScreenings
     * Сохранение данных в бд
     * Сначала проверка есть ли данные в бд
     * Затем проверяем их актуальность
     * Если данные акктуальны то записываем в бд
     * Результат - true, если база была обновлена, и false в противном случае
     * 
     * @return boolean
     */
    public function saveToDatabase(Array $filmScreenings) {
        $latest = FilmScreening::orderBy('date', 'desc')->first();
        $result = false;
        foreach ($filmScreenings as $filmScreening) {
            if($latest == null || self::validateDate($latest->date, $filmScreening['date'])) {
                $result = true;
                FilmScreening::create([
                    'cinema_hall' => $filmScreening['cinema_hall'],
                    'date' => Carbon::createFromFormat("j F Y H:i", $filmScreening['date']),
                    'film_name' => $filmScreening['film_name'],
                    'price' => $filmScreening['price'],
                ]);
            }
        }
        return $result;
    }

}