<?php

namespace App\Support;

use App\FilmScreening;
use Carbon\Carbon;
use PhpParser\Node\Stmt\TryCatch;

class CinemaParser
{
    protected $xpath;

    const URL = 'https://cityopen.ru/afisha/kinoteatr-salavat/';

    protected static $ru_month = array( 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря' );
    protected static $en_month = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
    
    function __construct() {
        // Нужно обработать ошибки проверить валидность urla
        $html = new \DOMDocument();
        @$html->loadHTMLFile(self::URL);
        $this->xpath = new \DOMXPath($html);
    }

    /**
     * 
     * Парсинг данных с сайта (https://cityopen.ru/afisha/kinoteatr-salavat/): 
     * Дата, 
     * Название Зала, 
     * Время начала, 
     * Название фильма, 
     * Цена
     * 
     * @return Array
     */
    public function parse() {
        $HALLS = 2;
        $TABLES_PER_DAY = 2*$HALLS + 1;
        $data = [];
        $tables = $this->xpath->query(".//*[contains(@class, 'td-page-content')]/table");
        // Цикл по дням
        for ($l=0 ; $l < $tables->length ; $l= $l + $TABLES_PER_DAY) {
            // Получаем день
            $dayTable = $tables[$l];
            $day =  utf8_decode($this->xpath->query("tbody/tr/td", $dayTable)[1]->nodeValue);
            // Цикл по залам
            for ($i = $l + 1; $i <= $l + 1 + $HALLS; $i = $i + 2) { 
                $hallTable = $tables[$i];
                $hall =  utf8_decode($this->xpath->query("tbody/tr/td/span/strong", $hallTable)[0]->nodeValue);
                $filmScreeningsTable = $tables[$i+1];
                $tr =  $this->xpath->query("tbody/tr", $filmScreeningsTable);
                // Цикл по показам
                for ($j=1 ; $j < $tr->length ; $j++ ) {
                    $filmScreening = [];
                    $td = $this->xpath->query("td", $tr[$j]);
                    $dateTimeStr =  $day." ".utf8_decode($td[0]->nodeValue);
                    $filmScreening['cinema_hall'] = $hall;
                    $filmScreening['date'] = str_replace(self::$ru_month, self::$en_month, $dateTimeStr);
                    $filmScreening['film_name'] = utf8_decode($td[1]->nodeValue);
                    $filmScreening['price'] = (float)$td[3]->nodeValue;
                    array_push($data , $filmScreening);
                }

            }
        }
        return $data;
    }
}