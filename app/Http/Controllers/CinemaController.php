<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support\CinemaParser;
use App\Support\CinemaParserDataBase;
use App\FilmScreening;
use Carbon\Carbon;

class CinemaController extends Controller
{
    /**
     * Обновление базы данных
     *
     * @return JSON
     */
    public function updateData() {
        $cinemaParser = new CinemaParser();
        $parsedData = $cinemaParser->parse();

        if(empty($parsedData)){
            return response()->json([
                'message' => "No data is parsed!",
                'success' => false
            ]);
        }

        $cinemaParserDataBase = new CinemaParserDataBase();
        $saveResult = $cinemaParserDataBase->saveToDatabase($parsedData);

        if($saveResult == false){
            return response()->json([
                'message' => 'Database is in actual state. No update is needed.',
                'success' => true
            ]);
        }

        return response()->json([
            'message' => "Database has been successfully updated!",
            'success' => true
        ]);

    }

    /**
     * Получение данных из базы и представление их в виде таблицы
     * 
     * Если передана строка поиска, то поиск по ней в бд
     *
     * @param  Request $request
     *
     * @return view
     */
    public function getData(Request $request) {
        $query = FilmScreening::all();
        $q = $request['search'];
        if ($q) {
            $query = FilmScreening::where('cinema_hall', $q)
                        ->orWhere('cinema_hall', $q)
                        ->orWhere('film_name', $q);
            if (is_numeric($q)) {
                $query = $query->orWhere('price', $q);
            }
            if ((bool) strtotime($q)) {
                $query = $query->orWhere('date', Carbon::parse($q));
            }
            $query = $query->get();
        }
        return view('index',  ['filmScreenings' => $query]);
    }
    
}
